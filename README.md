# Integración de Rails con LaTeX

## Descripción

El objetivo del proyecto es integrar latex y rails para la generación de
documentos que sean visualmente atractivos.

## Dependencias

### LaTeX

Necestamos LaTeX para poder generar los documentos, desafortunadamente no hay un método estándar de instalación entre los distintos sistemas operativos, a continuación se lista un ejemplo de instalación en Linux, Windows y MacOS:

#### Linux

El ejemplo utilizará una distribución basada en Debian (Ubuntu/Mint, etc.), para instalar LaTeX debemos ejecutar:

```sh
sudo apt update
sudo apt install texlive-full
```

#### Windows

Para este sistema operativo es necesario instalar el gestor de paquetes llamado (MikTeX)[https://miktex.org/download], ejecutar el instalador y escoger las opciones mostradas cuando se llegue a esta pantalla:

![Opciones de instalación en Windows](img/basic-miktex-settings.png)

Y seguir con el instalador como normalmente se seguiría. Para referencia (aquí)[https://miktex.org/howto/install-miktex] se encuentra la guía oficial de instalación

#### Mac

Para este sistema operativo tienes que instalar MacTeX descargando el archivo (pkg)[https://www.tug.org/mactex/mactex-download.html] correspondiente y dando doble clic para instalarlo.

## Instalación

Sólo debes ejecutar los siguientes comandos:

```sh
git clone git@gitlab.com:camachojua/rails-latex-test.git
cd rails-latex-test
bundle install
rails db:migrate
./bin/setup
```

## Gemas utilizadas

Aquí se muestran las gemas probadas en orden cronológico

- `rails-latex` https://github.com/amagical-net/rails-latex
- Nuestra gema

## Gemas ganadoras

TODO: determinar cuál gema funciona mejor con Rails
